-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 22, 2019 at 01:44 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `larticles_api`
--

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE `articles` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`id`, `title`, `body`, `created_at`, `updated_at`) VALUES
(1, 'Unde veritatis ut dolorum.', 'Omnis ut et optio itaque. Pariatur cum architecto occaecati quis saepe ipsum fugit. Placeat voluptatem dolorem praesentium. Omnis minima asperiores ab est.', '2019-03-30 14:21:12', '2019-03-30 14:21:12'),
(2, 'Langue', 'Parle Espanol nol', '2019-03-30 14:21:12', '2019-03-31 11:07:46'),
(4, 'Updated Title Now', 'Updated Body', '2019-03-30 14:21:12', '2019-03-31 10:37:21'),
(5, 'Qui saepe ratione quam nulla molestiae.', 'Distinctio praesentium et quidem dolore unde ut cupiditate accusantium. Reiciendis ipsum soluta omnis quos. Hic aliquam cum voluptatem. Sapiente qui doloribus et et est porro illum.', '2019-03-30 14:21:13', '2019-03-30 14:21:13'),
(6, 'Minus vero ut ipsum.', 'Rerum ea qui molestiae error. Quia enim dolor distinctio quidem sit pariatur qui. Eaque vel animi sint ducimus. Et impedit ipsam illo qui.', '2019-03-30 14:21:13', '2019-03-30 14:21:13'),
(7, 'Dolorem ut cumque qui nesciunt ut.', 'Quo dolorem occaecati et aliquam. Quae et voluptatem eos quas et labore laudantium. Natus rerum natus assumenda aut voluptas eligendi maiores. Et sint qui dolores molestiae quia.', '2019-03-30 14:21:13', '2019-03-30 14:21:13'),
(8, 'Est consequatur officiis odio quia corporis.', 'Accusantium ut doloremque et. Architecto magnam hic consequatur ut exercitationem.', '2019-03-30 14:21:13', '2019-03-30 14:21:13'),
(9, 'Porro maxime et maxime.', 'Doloribus tenetur molestiae dicta. Qui fuga dolor velit suscipit fuga beatae. Nostrum sint qui quia odio odio laboriosam id.', '2019-03-30 14:21:13', '2019-03-30 14:21:13'),
(10, 'Hic numquam minima est ullam velit eius.', 'Et explicabo ut quam sed. Aut a voluptatem rem dolor perspiciatis reprehenderit aut. Placeat nobis aut ipsa error molestias.', '2019-03-30 14:21:13', '2019-03-30 14:21:13'),
(11, 'Ut doloribus velit est laudantium suscipit quod.', 'Quis hic dolor tenetur in molestias. Facilis aut tenetur sunt nihil consequatur qui. Voluptate consequatur voluptas praesentium sed dignissimos pariatur. Incidunt fugiat voluptas perspiciatis.', '2019-03-30 14:21:13', '2019-03-30 14:21:13'),
(12, 'Ut iste perferendis laborum.', 'Est perferendis porro saepe id. Vel et porro cumque quisquam voluptatibus porro. Ab voluptates harum nobis inventore voluptas. Temporibus repellat sequi vero voluptatum.', '2019-03-30 14:21:13', '2019-03-30 14:21:13'),
(13, 'Non quod consectetur autem facere eaque.', 'Laborum et consequatur rerum et. Minus fugit error et doloremque.', '2019-03-30 14:21:14', '2019-03-30 14:21:14'),
(14, 'Nesciunt odio corporis deleniti expedita alias.', 'Perspiciatis ea similique aliquid alias dolorem ab laudantium. Distinctio ut nostrum expedita deleniti. Aut ut qui aperiam rerum corrupti culpa qui velit.', '2019-03-30 14:21:14', '2019-03-30 14:21:14'),
(15, 'Voluptatum voluptate cum maxime praesentium eum.', 'Aliquam dolor culpa et animi temporibus aliquid. Numquam enim doloribus esse molestiae quia pariatur. Dignissimos omnis officiis temporibus blanditiis. Recusandae sint non sunt nulla.', '2019-03-30 14:21:14', '2019-03-30 14:21:14'),
(16, 'Iusto animi asperiores sequi quo pariatur totam.', 'Deleniti aut iusto reprehenderit in earum quos. Harum quo aut ut temporibus voluptatem sunt. Animi nihil et eveniet odit odio voluptatum voluptas natus.', '2019-03-30 14:21:14', '2019-03-30 14:21:14'),
(17, 'Odit animi hic sit quisquam.', 'Magni et est hic rem iure qui molestiae. Aliquid eum quis totam est consequatur. Sed sed maxime rerum quia saepe omnis.', '2019-03-30 14:21:14', '2019-03-30 14:21:14'),
(18, 'Magni odio illo harum temporibus odio ut et.', 'Cumque ex ut temporibus iure repudiandae. Quisquam dolor praesentium aperiam a et ut. Vitae voluptatem repudiandae sed exercitationem repellendus.', '2019-03-30 14:21:14', '2019-03-30 14:21:14'),
(19, 'Ratione repellat dolor in.', 'Illo dolores rerum ratione quis in delectus. Veniam omnis vitae soluta sit qui. Consequuntur aut est id aliquid itaque nulla fugit minima.', '2019-03-30 14:21:14', '2019-03-30 14:21:14'),
(20, 'Veritatis iure placeat enim molestiae.', 'Est autem amet illum dolor. Voluptatem earum tempore tempora nostrum quibusdam. Eveniet dolor sint corporis a totam qui.', '2019-03-30 14:21:14', '2019-03-30 14:21:14'),
(21, 'Deserunt incidunt sunt mollitia atque.', 'Nam ea quod quia id maxime. Autem maiores quisquam id minima blanditiis voluptate pariatur. Vitae et placeat aliquid tempore.', '2019-03-30 14:21:14', '2019-03-30 14:21:14'),
(22, 'Eum et deserunt non sed molestiae cupiditate qui.', 'Velit voluptatem animi sed ipsum. Est architecto odit vero tenetur.', '2019-03-30 14:21:14', '2019-03-30 14:21:14'),
(23, 'Eum sit est blanditiis nihil.', 'Suscipit neque iste rerum. Maxime officiis aut non tenetur ut quam. Deleniti aut sit soluta minima quo aut. Quisquam officiis pariatur sunt fugiat.', '2019-03-30 14:21:14', '2019-03-30 14:21:14'),
(24, 'Odio ea doloremque asperiores officia ullam est.', 'Ut itaque sed distinctio architecto temporibus. Aperiam rem eaque nobis omnis. Unde rerum eius exercitationem expedita qui id est eum. At omnis non nobis doloremque expedita ipsum.', '2019-03-30 14:21:15', '2019-03-30 14:21:15'),
(25, 'Optio perferendis excepturi asperiores sunt.', 'Et ipsa nisi magnam qui ullam. Optio et voluptatum autem exercitationem.', '2019-03-30 14:21:15', '2019-03-30 14:21:15'),
(26, 'Necessitatibus sint sit animi.', 'Tempore quo reiciendis omnis vero minima quasi quam possimus. Illo enim sed adipisci amet. Magni quos sint amet ipsam.', '2019-03-30 14:21:15', '2019-03-30 14:21:15'),
(27, 'Magni minus a nulla tempora et cupiditate aut.', 'Fugit voluptatem impedit qui rerum omnis consequatur. Autem nesciunt aut fugiat officiis doloribus eos non. Qui rerum asperiores illo.', '2019-03-30 14:21:15', '2019-03-30 14:21:15'),
(28, 'Id saepe aliquid distinctio.', 'Repudiandae voluptas soluta et voluptatibus laborum. Laudantium odio officia minus amet. Numquam harum voluptatibus consequatur eos ut veritatis.', '2019-03-30 14:21:15', '2019-03-30 14:21:15'),
(29, 'Dicta impedit non est officia aut laudantium.', 'Eum veritatis iusto placeat commodi quidem nostrum quasi magnam. Magni adipisci recusandae ipsa laboriosam itaque. Unde harum officia hic voluptatibus veritatis ducimus et.', '2019-03-30 14:21:15', '2019-03-30 14:21:15'),
(30, 'Culpa velit quam praesentium fugiat et.', 'Aspernatur asperiores similique dolorem ut ratione. Non labore ducimus necessitatibus quam. Ut totam facilis deleniti deleniti possimus enim quo.', '2019-03-30 14:21:15', '2019-03-30 14:21:15'),
(31, 'Test Title', 'Test Body', '2019-03-30 16:24:13', '2019-03-30 16:24:13'),
(32, 'Test', 'Body Has been changed', '2019-03-31 10:44:29', '2019-03-31 10:45:31'),
(33, 'Espanol Langue', 'Espanol Langue n', '2019-03-31 10:52:23', '2019-03-31 10:52:23');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_03_30_164232_create_articles_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
