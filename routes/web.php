<?php
use \Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/**ROUTES FOR THE TEST CASE**/
Route::get('/', 'Test\TestController@index');
Route::get('/test/processing', 'Test\TestController@processing')->name('process');
Route::get('/test/pay_verification', 'Test\TestController@pay_verification')->name('verification');
Route::resource('/test', 'Test/TestController');
Route::get('/testblade', 'Test\TestbladeController@first');
/*Route::get('/alerts/{name}/{gae}', function($name, $age){
    return view('alert', compact([
        'name', 'age'
    ]));
});   #defined in the route directory*/
Route::get('/alerts/{name}/{gae}', 'Test\TestbladeController@first')->name('st');
/********************************************************************/
