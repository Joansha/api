<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
//LIST ARTICLES
Route::get('articles', 'ArticleController@index');

//LIST SINGLE ARTICLE
Route::get('articles/{id}', 'ArticleController@show');

//CREATE NEW ARTICLE -which is a post request, it stores or add data to db
Route::post('articles', 'ArticleController@store');

//UPDATE ARTICLE - and is a put request
Route::put('articles', 'ArticleController@store');

//DELETE ARTICLE - delete request
Route::delete('articles/{id}', 'ArticleController@destroy');
