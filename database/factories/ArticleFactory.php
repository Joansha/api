<?php

use Faker\Generator as Faker;

$factory->define(App\Article::class, function (Faker $faker) {
    return [
        'title' => $faker->text(50), //50 is th number of characters or length
        'body' => $faker->text(200)
    ];
});
