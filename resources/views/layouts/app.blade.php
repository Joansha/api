<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="/css/test.css"> <!--For External css defined in public-->
    <script src="js/test.js"></script> <!--For Js defined in public-->
    @yield('style')
</head>

<body>
@section('side')
    Hello Human
    @show

@yield('table')
@yield('school')
</body>
</html>
