<h3>PAYMENT VERIFICATION TEST RESULT</h3>
<?php

$val = "demo"; //assigned iPay Vendor ID... hard code it here.
/*
these values below are picked from the incoming URL and assigned to variables that we
will use in our security check URL
*/
$val1 = $_GET["id"];
$val2 = $_GET["ivm"];
$val3 = $_GET["qwh"];
$val4 = $_GET["afd"];
$val5 = $_GET["poi"];
$val6 = $_GET["uyt"];
$val7 = $_GET["ifd"];

$ipnurl = "https://www.ipayafrica.com/ipn/?vendor=".$val."&id=".$val1."&ivm=".
    $val2."&qwh=".$val3."&afd=".$val4."&poi=".$val5."&uyt=".$val6."&ifd=".$val7;
$fp = fopen($ipnurl, "rb");
$status = stream_get_contents($fp, -1, -1);
fclose($fp);
echo $ipnurl;

//the value of the parameter “vendor”, in the url being opened above, is your iPay assigned

//this is the correct iPay status code corresponding to this transaction.
//Use it to validate your incoming transaction(not the one supplied in the incoming url)

//continue your shopping cart update routine code here below....
//then redirect to to the customer notification page here...
?>
	