<h2 align="center"><u>{{ $title }}</u></h2>
{!! Form::open(['action' => 'Test\TestController@processing', 'method' => 'GET']) !!}
<table style="width: 60%; text-align: center" >

    @csrf
    <thead>
    <tr>
        <th>ITEM NAME</th>
        <th>QUANTITY</th>
        <th>PRICE</th>
        <th>TOTAL(KSH)</th>
    </tr>
    </thead>
    <tbody>
    @foreach($price_promotion as $key => $value)
    <tr>
        <td>{{ $value }}</td>
        <!--random_int() function is used to randomly generate the numbers 1-9 -->
        <td><input type="number" value="{{ $x = random_int(1,9) }}" disabled id="digit" name="promote" style="width: 15%" ></td>
        <td>{{  $key  }}</td>
        <td><p>{{ $y = $x * $key }}</p></td>
        <?php
        $sum = $sum + $y
        ?>
    </tr>
    @endforeach

    <tr><td>
            <hr>
            <strong>Telephone:</strong><br>
            <input type="number" name="phone" style="width: 60%; padding: 2%; border-radius: 5%"
                   placeholder="ENTER PHONE NUMBER" required>
            <h3>TOTAL: {{ $sum }}</h3>

            <input type="hidden" name="sums" value="{{ $sum }}">
            <input type="submit" name="submit" value="CHECKOUT" style="background-color: darkblue; padding: 2%;
            border-radius: 10%; font-weight: bold; font-size: 1.3em; margin-left: 100%; color: gainsboro" />
    </td></tr>
    <tr><td>
            <b>NOTE:</b> The <b>Quantity</b> is automatically generated.<br>
                    Refresh page to confirm, then <b>Totals</b> calculated
        </td></tr>
    </tbody>
</table>
{!! Form::close() !!}
