<?php

namespace App\Http\Middleware;

use Closure;

class CheckAge
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->age < 200){
            //Performed before request is handled
            return redirect('admin/users');
        }
        elseif ($request->age == 200){
            return redirect('guest');
        }
        else{
            return $next($request);
        }
        //return $next($request);

        /*
             public function handle($request, Closure $next)
                {
                    $response = $next($request);

                    // Handles request before performing middleware

                    return $response;
                }
         * */
    }
}
