<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Article extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request); -returns the complete api array of data
        return [
            'id' => $this->id,
            'title' =>$this->title,
            'body' => $this->body
        ];
    }

    public function with($request) //Inorder to return with something
    {  //u can do whatever u want here
        return [
            'version' => '1.0.0',
            'author_name' =>'Shish'
        ];
    }
    public function withResponse($request, $response)
    {
        $response->header('Content-Type', 'application/json');
    }
}
