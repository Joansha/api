<?php

namespace App\Http\Controllers\Test;

use App\Http\Requests\testRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = "Dummy Test Data";
        //---An array of items on promotion--
        $promotion = ['Fridge', 'Washing Machine', 'Chair', 'Television', 'Samsung J5'];
        //--The item prices array
        $price = [12389, 30000, 400, 10000, 12000];
        $price_promotion = array_combine($price ,$promotion);
        $sum = 0;


        return view('test.index', compact([
            'price_promotion', 'title', 'sum'
        ]));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(testRequest $request)
    {
        $validated = $request->validated(); //retrieve validated input data
    }

    public function processing(Request $request){
        $live = "0";
        $oid = rand(100,1000);
        $inv = random_int(100000, 100000000);
        $ttl = $request->input('sums');
        $phone = $request->input('phone');
        $email = "jo@gmail.com";
        $vid = "demo";
        $curr = "KES";
		$_SERVER["HTTP_HOST"] = 'http://';
		$_SERVER["REQUEST_URI"] = 'precious.test/test/pay_verification';
        $cbk = $_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"];
        $cst = "1";
        $crl = "0";
        return view('test.processing', compact([
            'live', 'phone', 'inv', 'oid', 'ttl', 'email', 'vid', 'curr', 'cbk', 'cst', 'crl'
        ]));
    }

    public function pay_verification(){
		$_SERVER["HTTP_HOST"] = 'https://';
		$_SERVER["REQUEST_URI"] = 'www.google.com';
		$status = $_GET['status'];
		if($status == 'aei7p7yrx4ae34'){
        return redirect()->route($_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"]);
		} else {
			return redirect('process');
		}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
