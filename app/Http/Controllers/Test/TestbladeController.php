<?php

namespace App\Http\Controllers\Test;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TestbladeController extends Controller
{
    public function first(Request $res, $name, $age){
        $url = $res->url();
        $testarray = array('red', 'pink', 'blue');
        return view('alert', compact([
            'name', 'age', 'url', 'testarray'
        ]));
    }
}
