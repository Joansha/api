The Test system runs on Laravel
The controller used is the TestController and the blade files in Test folder in View

Prerequisites
PHP and xampp or WAMPP must be installed in the system or use 
Laravel Homestead virtual machine, which is a full package.

Create a virtual Environment
Open notepad as administrator to be able to save changes.
Go to: File->open->C:\xampp\apache\conf\extra, open vhosts or httpd-vhost
-If you are in the extra directory and cannot see anything, select the 'All File' option
Paste these codes at the end of the file and save - Notepad should be running as admin

<VirtualHost *:80>
    DocumentRoot "C:/xampp/htdocs/loyonesemin/public" #path to the unzipped file's public folder
    ServerName precious.test
</VirtualHost>

Go to: File->open->C:\Windows\System32\drivers\etc
select 'All File' option and open hosts file, copy, paste, and save file
127.0.0.1       localhost
127.0.0.1       precious.test

-If all these while your xampp controller was running, restart the processes again
-If you dont restart, it returns an error or wont load
-Unzip the zipped file inside htdocs
-Load the url 'precious.test' on the browser
-If all the above steps were followed as required, the system should respond
-If u get a 'Maximum execution time ...' error, just refresh the page, the system
may be running slow. The zipped file sent is still in developer mode